﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sample {

    public class ErrorMessageWindow : SingletonMonoBehaviour<ErrorMessageWindow> {

        [SerializeField] RectTransform window;
        [SerializeField] Text messageText;

        public void SetMessageText(string text) {
            window.gameObject.SetActive(true);
            messageText.text = text;
        }

        public void Close() {
            window.gameObject.SetActive(false);
            messageText.text = string.Empty;
        }

    }

}
