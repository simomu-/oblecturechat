﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sample {

    public class LoginManager : MonoBehaviour {

        public static string userID;

        [SerializeField] StageSelectScroll selectScroll;

        [SerializeField] InputField addressInput;
        [SerializeField] InputField userIDInput;
        [SerializeField] InputField passwordInput;
        [SerializeField] MessageManager messageManager;

        void ClearLoginInput() {
            userIDInput.text = string.Empty;
            passwordInput.text = string.Empty;
        }

        public void SetAddress() {
            ApiClient.Instance.SetAddress(addressInput.text);
        }

        public void CreateUser() {
            RequestCreateUser param = new RequestCreateUser();
            param.userID = userIDInput.text;
            param.password = passwordInput.text;
            ApiClient.Instance.ResponseCreateUser = (ResponseCreateUser response) => {
                if (response.message == "Success") {
                    LoginUser();
                } else {
                    Debug.LogError(response.message);
                    ErrorMessageWindow.Instance.SetMessageText(response.message);
                    ClearLoginInput();
                }
            };
            ApiClient.Instance.RequestCreateUser(param);
        }

        public void LoginUser() {
            RequestLogin param = new RequestLogin();
            param.userID = userIDInput.text;
            param.password = passwordInput.text;
            ApiClient.Instance.ResponseLoginUser = (ResponseLogin response) => {
                if (response.message == "Success") {
                    userID = response.user.userID;
                    ClearLoginInput();
                    messageManager.Init();
                    selectScroll.MoveRight();
                } else {
                    Debug.LogError(response.message);
                    ErrorMessageWindow.Instance.SetMessageText(response.message);
                    ClearLoginInput();
                }
            };
            ApiClient.Instance.RequestLoginUser(param);
        }    
    }
}
