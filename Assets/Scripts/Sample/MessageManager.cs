﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sample {
    public class MessageManager : MonoBehaviour {

        public static int roomID;

        [SerializeField] RectTransform roomContent;
        [SerializeField] RoomElement roomElement;
        [SerializeField] InputField createRoomNameInput;
        [SerializeField] Text messageText;
        [SerializeField] InputField messageInput;
        [SerializeField] Text roomNameText;
        [SerializeField] Text userListText;

        public void Init() {
            RequestRoom();
        }

        public void RequestRoom() {
            ClearRoomList();
            RequestShowRoom param = new RequestShowRoom();
            ApiClient.Instance.ResponseShowRoom = (ResponseShowRoom response) => {
                for (int i = 0; i < response.rooms.Count; i++) {
                    RoomElement element = Instantiate(roomElement) as RoomElement;
                    element.Init(response.rooms[i],this);
                    element.transform.SetParent(roomContent.transform, false);
                    element.gameObject.SetActive(true);
                }
            };
            ApiClient.Instance.RequestShowRoom(param);
        }

        public void CreateRoom() {
            RequestCreateRoom param = new RequestCreateRoom();
            param.userID = LoginManager.userID;
            param.roomName = createRoomNameInput.text;
            ApiClient.Instance.ResponseCreateRoom = (ResponseCreateRoom response) => {
                if (response.message == "Success") {
                    createRoomNameInput.text = string.Empty;
                    RequestRoom();
                } else {
                    Debug.LogError(response.message);
                    ErrorMessageWindow.Instance.SetMessageText(response.message);
                }
            };
            ApiClient.Instance.RequestCreateRoom(param);
        }

        public void RequestMessage() {
            RequestShowMessage param = new RequestShowMessage();
            param.roomID = roomID;
            param.count = 100;
            ApiClient.Instance.ResponseShowMessage = (ResponseShowMessage response) => {
                messageText.text = string.Empty;
                for (int i = 0; i < response.chats.Count; i++) {
                    messageText.text += string.Format("{0} {1}\n{2}\n\n",
                        response.chats[i].userID,
                        response.chats[i].time,
                        response.chats[i].message);
                }
            };
            ApiClient.Instance.RequestShowMessage(param);
        }

        public void SendMessage() {
            RequestSendMessage param = new RequestSendMessage();
            param.userID = LoginManager.userID;
            param.roomID = roomID;
            param.message = messageInput.text;
            ApiClient.Instance.ResponseSendMessage = (ResponseSendMessage response) => {
                if (response.message == "Success") {
                    messageInput.text = string.Empty;
                    RequestMessage();
                } else {
                    Debug.LogError(response.message);
                    ErrorMessageWindow.Instance.SetMessageText(response.message);
                }
            };
            ApiClient.Instance.RequestSendMessage(param);
        }

        public void ShowUserList() {
            userListText.text = string.Empty;
            RequestUsersInRoom param = new RequestUsersInRoom();
            param.roomID = roomID;
            ApiClient.Instance.ResponseUsersInRoom = (ResponseUsersInRoom response) => {
                for (int i = 0; i < response.users.Count; i++) {
                    userListText.text += response.users[i].userID + "\n";
                }
            };
            ApiClient.Instance.RequestUsersInRoom(param);
        }

        public void Exit() {
            ClearRoomList();
            ClearMessage();
            LoginManager.userID = string.Empty;
            roomID = 0;
        }

        public void ClearMessage() {
            messageText.text = string.Empty;
        }

        public void ClearRoomList() {
            for (int i = 1; i < roomContent.childCount; i++) {
                Destroy(roomContent.GetChild(i).gameObject);
            }
        }

    }
}
