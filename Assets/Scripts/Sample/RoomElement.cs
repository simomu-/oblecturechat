﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sample {
    public class RoomElement : MonoBehaviour {

        [SerializeField] Text roomNameText;
        [SerializeField] Text authorText;
        [SerializeField] Button deleteButton;
        [SerializeField] StageSelectScroll selectScroll;
        [SerializeField] bool androidDebudMode;

        int roomID;
        MessageManager messageManager;

        public void Init(Room room, MessageManager manager) {
            roomNameText.text = room.roomName;
            authorText.text = "auther:" + room.userID;
            deleteButton.interactable = (room.userID == LoginManager.userID);
            this.roomID = room.roomID;
            messageManager = manager;         
        }

        public void JoinRoom() {
            RequestJoinRoom param = new RequestJoinRoom();
            param.userID = LoginManager.userID;
            param.roomID = roomID;
            ApiClient.Instance.ResponseJoinRoom = (ResponseJoinRoom response) => {
                if (response.message == "Success") {
                    MessageManager.roomID = roomID;
                    messageManager.RequestMessage();
                    if(Application.platform == RuntimePlatform.Android || androidDebudMode) {
                        selectScroll.MoveRight();
                    }
                } else {
                    Debug.LogError(response.message);
                    ErrorMessageWindow.Instance.SetMessageText(response.message);
                }
            };
            ApiClient.Instance.RequestJoinRoom(param);
        }

        public void DeleteRoom() {
            RequestDeleteRoom param = new RequestDeleteRoom();
            param.roomID = roomID;
            param.userID = LoginManager.userID;
            ApiClient.Instance.ResponseDeleteRoom = (ResponseDeleteRoom response) => {
                if (response.message == "Success") {
                    messageManager.ClearMessage();
                    MessageManager.roomID = 0;
                    messageManager.RequestRoom();
                } else {
                    Debug.LogError(response.message);
                    ErrorMessageWindow.Instance.SetMessageText(response.message);
                }
            };
            ApiClient.Instance.RequestDeleteRoom(param);
        }

    }
}

