﻿using UnityEngine;
using UnityEngine.Events;

public class ApiClient : SingletonMonoBehaviour<ApiClient> {

    string address;
    Requester requester;

    public override void Awake() {
        base.Awake();
        requester = new Requester(this);
    }

    public void SetAddress(string address) {
        this.address =  "http://" + address + "/OBLectureChat/";
    }

    /// <summary>
    /// RequestCreateUser
    /// /Users/addへPOSTリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestCreateUser(RequestCreateUser param) {
        string url = address + "Users/add.json";
        requester.Post<RequestCreateUser, ResponseCreateUser>(url, param, ResponseCreateUser);
    }

    /// <summary>
    /// ResponseCreateUser
    /// /Users/addへPOSTでリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseCreateUser> ResponseCreateUser;


    /// <summary>
    /// RequestLoginUser
    /// /Users/loginへPOSTリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestLoginUser(RequestLogin param) {
        string url = address + "Users/login.json";
        requester.Post<RequestLogin, ResponseLogin>(url, param, ResponseLoginUser);
    }

    /// <summary>
    /// ResponseLogin
    /// /Users/loginへPOSTリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseLogin> ResponseLoginUser;

    /// <summary>
    /// RequestCreateRoom
    /// /Rooms/addへPOSTリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestCreateRoom(RequestCreateRoom param) {
        string url = address + "Rooms/add/.json";
        if (string.IsNullOrEmpty(param.roomName)) {
            param.roomName = "No Name"; 
        }
        requester.Post<RequestCreateRoom, ResponseCreateRoom>(url, param, ResponseCreateRoom);
    }

    /// <summary>
    /// ResponseCreateRoom
    /// /Rooms/addへPOSTリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseCreateRoom> ResponseCreateRoom;

    /// <summary>
    /// RequestShowRoom
    /// /Rooms/indexへGETリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestShowRoom(RequestShowRoom param) {
        string url = address + "Rooms/index.json";
        requester.Get<ResponseShowRoom>(url, ResponseShowRoom);
    }

    /// <summary>
    /// ResponseShowRoom
    /// /Rooms/indexへGETリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseShowRoom> ResponseShowRoom;

    /// <summary>
    /// RequestJoinRoom
    /// /Rooms/edit/:userIDへPOSTリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestJoinRoom(RequestJoinRoom param) {
        string url = address + string.Format("Users/edit/{0}.json", param.userID);
        requester.Post<RequestJoinRoom, ResponseJoinRoom>(url, param, ResponseJoinRoom);
    }

    /// <summary>
    /// ResponseJoinRoom
    /// /Rooms/edit/:userIDへPOSTリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseJoinRoom> ResponseJoinRoom;

    /// <summary>
    /// RequestUsersInRoom
    /// /Rooms/roomUsers/:roomIDへGETリクエストリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestUsersInRoom(RequestUsersInRoom param) {
        string url = address + string.Format("Users/roomUsers/{0}.json", param.roomID);
        requester.Get<ResponseUsersInRoom>(url, ResponseUsersInRoom);
    }

    /// <summary>
    /// ResponseUsersInRoom
    /// /Rooms/roomUsers/:roomIDへGETリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseUsersInRoom> ResponseUsersInRoom;

    /// <summary>
    /// RequestDeleteRoom
    /// /Rooms/delete/:roomID/:userIDへDELETEリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestDeleteRoom(RequestDeleteRoom param) {
        string url = address + string.Format("Rooms/delete/{0}/{1}.json", param.roomID, param.userID);
        requester.Delete<ResponseDeleteRoom>(url, ResponseDeleteRoom);
    }

    /// <summary>
    /// ResponseDeleteRoom
    /// /Rooms/delete/:roomID/:userIDへDELETEリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseDeleteRoom> ResponseDeleteRoom;

    /// <summary>
    /// RequestSendMessage
    /// /Messages/addへPOSTリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestSendMessage(RequestSendMessage param) {
        string url = address + "Messages/add.json";
        requester.Post<RequestSendMessage, ResponseSendMessage>(url, param, ResponseSendMessage);
    }
    
    /// <summary>
    /// ResponseSendMessage
    /// /Messages/addへPOSTリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseSendMessage> ResponseSendMessage;

    /// <summary>
    /// RequestShowMessage
    /// /Messages/view/:roomID/:countへGETリクエストを行う
    /// </summary>
    /// <param name="param"></param>
    public void RequestShowMessage(RequestShowMessage param) {
        string url = address + string.Format("Messages/view/{0}/{1}.json", param.roomID,param.count);
        requester.Get<ResponseShowMessage>(url, ResponseShowMessage);
    }

    /// <summary>
    /// ResponseShowMessage
    /// /Messages/view/:roomID/:countへGETリクエストを行った時のコールバック
    /// </summary>
    public UnityAction<ResponseShowMessage> ResponseShowMessage;
}
