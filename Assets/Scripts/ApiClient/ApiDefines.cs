﻿using System.Collections.Generic;

[System.Serializable]
public class RequestCreateUser {
    public string userID;
    public string password;
}

[System.Serializable]
public class User {
    public string userID;
    public string password;
    public int joining_room;
}

[System.Serializable]
public class ResponseCreateUser {
    public string message;
    public User user;
}

[System.Serializable]
public class RequestLogin {
    public string userID;
    public string password;
}

[System.Serializable]
public class ResponseLogin {
    public string message;
    public User user;
}

[System.Serializable]
public class RequestCreateRoom {
    public string userID;
    public string roomName;
}

[System.Serializable]
public class Room {
    public int roomID;
    public string roomName;
    public string userID;
    public string time;
}

[System.Serializable]
public class ResponseCreateRoom {
    public string message;
    public Room room;
}

[System.Serializable]
public class RequestShowRoom {
}

[System.Serializable]
public class ResponseShowRoom {
    public List<Room> rooms;
}

[System.Serializable]
public class RequestJoinRoom {
    public int roomID;
    public string userID;
}

[System.Serializable]
public class ResponseJoinRoom {
    public string message;
    public User user;
}

[System.Serializable]
public class RequestUsersInRoom {
    public int roomID;
}

[System.Serializable]
public class ResponseUsersInRoom {
    public List<User> users;
}

[System.Serializable]
public class RequestDeleteRoom {
    public int roomID;
    public string userID;
}

[System.Serializable]
public class ResponseDeleteRoom {
    public string message;
}

[System.Serializable]
public class RequestSendMessage {
    public int roomID;
    public string userID;
    public string message;
}

[System.Serializable]
public class Message {
    public int messageID;
    public string userID;
    public int roomID;
    public string time;
    public string message;
}

[System.Serializable]
public class ResponseSendMessage {
    public string message;
    public Message chat;
}

[System.Serializable]
public class RequestShowMessage {
    public int roomID;
    public int count = 20;
}

[System.Serializable]
public class ResponseShowMessage {
    public string message;
    public List<Message> chats;
}
