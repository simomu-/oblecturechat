﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class Requester {

    MonoBehaviour monoBehavior;

    public Requester(MonoBehaviour monoBehavior) {
        this.monoBehavior = monoBehavior;
    }

    /// <summary>
    /// Send
    /// 実際のHTTPリクエストを行う関数
    /// MonoBehavior.StartCoroutineを用いて呼び出す
    /// </summary>
    /// <typeparam name="RESPONSE">レスポンスデータの型</typeparam>
    /// <param name="url">リクエストURL</param>
    /// <param name="method">HTTPメソッド</param>
    /// <param name="callback">リクエストのコールバック</param>
    /// <param name="data">送信データ(jsonフォーマット)</param>
    /// <returns></returns>
    IEnumerator Send<RESPONSE>(string url, string method, UnityAction<RESPONSE> callback,string data = default(string)) {
        UnityWebRequest request = new UnityWebRequest(url);

        if (!string.IsNullOrEmpty(data)) {
            byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(data);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);

        }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.method = method;
        request.SetRequestHeader("Content-Type", "application/json");
        //Debug.Log("Send");
        yield return request.Send();

        if (request.isError) {
            Debug.LogError(request.error);
            yield break;
        }

        //Debug.Log(request.downloadHandler.text);

        if (request.downloadHandler.isDone) {
            try {
                RESPONSE response = JsonUtility.FromJson<RESPONSE>(request.downloadHandler.text);
                if (callback != null) {
                    callback(response);
                }
            } catch (System.ArgumentException e) {
                Debug.LogError(e.Message);
            }
        } else {
            Debug.LogError(request.error);
            yield break;
        }
    }

    /// <summary>
    /// Get
    /// GETリクエストを行う
    /// </summary>
    /// <typeparam name="RESPONSE">レスポンスデータの型</typeparam>
    /// <param name="url">リクエストURL</param>
    /// <param name="callback">レスポンスのコールバック</param>
    public void Get<RESPONSE>(string url, UnityAction<RESPONSE> callback) {
        monoBehavior.StartCoroutine(Send<RESPONSE>(url, UnityWebRequest.kHttpVerbGET, callback));
    }

    /// <summary>
    /// Post
    /// POSTリクエストを行う
    /// </summary>
    /// <typeparam name="SEND">送信データの型</typeparam>
    /// <typeparam name="RESPONSE">レスポンスデータの型</typeparam>
    /// <param name="url">リクエストURL</param>
    /// <param name="data">送信データ</param>
    /// <param name="callback">レスポンスのコールバック</param>
    public void Post<SEND,RESPONSE>(string url,SEND data, UnityAction<RESPONSE> callback) {
        string json = JsonUtility.ToJson(data);
        monoBehavior.StartCoroutine(Send<RESPONSE>(url, UnityWebRequest.kHttpVerbPOST, callback, json));
    }

    /// <summary>
    /// Delete
    /// DELETEリクエストを行う
    /// </summary>
    /// <typeparam name="RESPONSE">レスポンスデータの型</typeparam>
    /// <param name="url">リクエストURL</param>
    /// <param name="callback">レスポンスのコールバック</param>
    public void Delete<RESPONSE>(string url, UnityAction<RESPONSE> callback) {
        monoBehavior.StartCoroutine(Send<RESPONSE>(url, UnityWebRequest.kHttpVerbDELETE, callback));
    }

}
